provider "kubernetes" {
  config_path    = "/usr/share/kubeconfig/config"
  config_context = "kubernetes-admin@kubernetes"
}

resource "kubernetes_namespace" "example" {
  metadata {
    name = "my-first-namespace"
  }
}

